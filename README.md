# Android Companion App

[![pipeline status](https://git.card10.badge.events.ccc.de/card10/companion-app-android/badges/master/pipeline.svg)](https://git.card10.badge.events.ccc.de/card10/companion-app-android/pipelines)

## Download

Latest Build binary from ci here:

[Debug-Binary](https://git.card10.badge.events.ccc.de/card10/companion-app-android/-/jobs/artifacts/master/raw/app/build/outputs/apk/debug/app-debug.apk?inline=false&job=assembleDebug)
